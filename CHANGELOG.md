# Change Log


## Next release

## [[v0.2?](https://github.com/terraform-aws-modules/terraform-aws-eks/compare/v0.2...HEAD)] - 2019-03-12]

### Added

- Added Istio
- Added Helm Installer
- Enable sidecar injection
- Fixed aws-iam-authenticator

### Changed

 - Write your awesome change here (by @you)

# History

## [[v0.1](https://github.com/terraform-aws-modules/terraform-aws-eks/compare/v0.1...master)] - 2019-03-11]

### Added

- Initial Commits

